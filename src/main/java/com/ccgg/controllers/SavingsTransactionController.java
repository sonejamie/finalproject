package com.ccgg.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ccgg.beans.SavingsTransaction;
import com.ccgg.dao.SavingsTransactionDao;
import com.ccgg.http.Response;
import com.ccgg.service.TransactionService;

@RestController
@RequestMapping("/savingstransaction")
public class SavingsTransactionController {
	
	@Autowired
	SavingsTransactionDao savingsTransactionDao;
	
	@Autowired
	TransactionService transactionService;
	
	@GetMapping
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	public List<SavingsTransaction> getSavingsTransaction(){
		return savingsTransactionDao.findAll();
	}
	
	@PostMapping
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	public Response addSavingsTransaction(@RequestBody SavingsTransaction savingsTransaction) {
		return transactionService.addSavingsTransaction(savingsTransaction);
	}
	
	@PutMapping("/edit/{id}")
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
	public Response updateSavingsTransaction(@PathVariable int id, @RequestBody SavingsTransaction savingsTransaction) {
		return transactionService.updateSavingsTransaction(savingsTransaction);
	}
	
	@DeleteMapping("/{id}")
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	public Response deleteSavingsTransaction(@PathVariable int id) {
		return transactionService.deletesavingsTransaction(id);
	}
	
	
	
	

}


