package com.ccgg.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ccgg.beans.PrimaryTransaction;
import com.ccgg.dao.PrimaryTransactionDao;
import com.ccgg.http.Response;
import com.ccgg.service.TransactionService;


@RestController
@RequestMapping("/primarytransaction")
public class PrimaryTransactionController {
	
	@Autowired
	PrimaryTransactionDao primaryTransactionDao;
	
	@Autowired
	TransactionService transactionService;
	
	
	@GetMapping
	public List<PrimaryTransaction> getPrimaryTransaction(){
		return primaryTransactionDao.findAll();
	}
	
	@PostMapping
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	public Response addPrimaryTransaction(@RequestBody PrimaryTransaction primaryTransaction) {
		return transactionService.addPrimaryTransaction(primaryTransaction);
	}
	
	@PutMapping("/edit/{id}")
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	public Response updatePrimaryTransaction(@PathVariable int id, @RequestBody PrimaryTransaction primaryTransaction) {
		return transactionService.updatePrimaryTransaction(primaryTransaction);
	}
	
	@DeleteMapping("/{id}")
	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	public Response deletePrimaryTransaction(@PathVariable int id) {
		return transactionService.deleteprimaryTransaction(id);
	}
	
	
	
	

}
