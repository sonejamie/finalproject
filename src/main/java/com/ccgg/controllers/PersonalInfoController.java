package com.ccgg.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ccgg.beans.PersonalInfo;
import com.ccgg.dao.PersonalInfoDao;
import com.ccgg.http.Response;
import com.ccgg.service.PersonalInfoService;

@RestController
@RequestMapping("/personal-info")
public class PersonalInfoController {
	
	
	@Autowired
	PersonalInfoDao personalInfoDao;
	
	@Autowired
	PersonalInfoService personalInfoService;
	
	@GetMapping
	public List<PersonalInfo> getpersonalInfo(){
		return personalInfoDao.findAll();
	}
	
	@PostMapping
	public Response postPersonalInfo(@RequestBody PersonalInfo personalInfo) {
		return personalInfoService.addPersonalInfo(personalInfo);
	}
	
	@PutMapping("/edit/{id}") 
	public Response putPersonalInfo(@RequestBody PersonalInfo personalInfo) {
		return personalInfoService.updatePersonalInfo(personalInfo);
	}
	
	@DeleteMapping("/{id}")
	public Response deletePersonalInfo(@PathVariable int id) {
		return personalInfoService.deletePersonalInfo(id);
	}
	
	
	

}
