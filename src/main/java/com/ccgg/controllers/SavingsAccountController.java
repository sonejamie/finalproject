package com.ccgg.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ccgg.beans.SavingsAccount;
import com.ccgg.dao.SavingsAccountDao;
import com.ccgg.http.Response;
import com.ccgg.service.AccountService;

@RestController
@RequestMapping("/savingsaccount")
public class SavingsAccountController {
	
	@Autowired
	AccountService accountService;
	
	@Autowired 
	SavingsAccountDao savingsAccountDao;
	
	@GetMapping
	public List<SavingsAccount> getaccount(){
		return savingsAccountDao.findAll();
	}
	
	@PostMapping
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	public Response addSavingsAccount(@RequestBody SavingsAccount savingsAccount) {
		return accountService.addSavingsAccount(savingsAccount);
	}
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	public Response deleteSavingsAccount(@PathVariable int id) {
		return accountService.deleteSavingsAccount(id);
	}
	
	

}
