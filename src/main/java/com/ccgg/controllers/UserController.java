package com.ccgg.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ccgg.beans.User;
import com.ccgg.dao.UserDao;
import com.ccgg.http.Response;
import com.ccgg.service.UserService;

@RestController()//recept API request
@RequestMapping("/users")
public class UserController {
	@Autowired
	UserDao userDao;

	@Autowired
	UserService userService;

	@Autowired
	PasswordEncoder passwordEncoder;

	@PreAuthorize("hasAuthority('ROLE_ADMIN')")//=addMatchers,只有amin才能访问
	@GetMapping
	public List<User> getusers(){
		return userDao.findAll();
	}

	@PostMapping
	public Response addUser(@RequestBody User user) {//json//requestBody规定格式
		
		return userService.register(user);
	}
	
	@PostMapping("/admin")
	public Response addAdmin(@RequestBody User user) {//json//requestBody规定格式
		return userService.registerAdm(user);
	}

	//@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")//改密码，登陆之后才行
	@PutMapping
	public Response changeUser(@RequestBody User user, Authentication authentication) {
		return userService.changePassword(user, authentication);
	}

	@DeleteMapping("/{id}")//变量传参
	public Response deleteUser(@PathVariable int id) {
		return userService.deleteUser(id);
	}

}