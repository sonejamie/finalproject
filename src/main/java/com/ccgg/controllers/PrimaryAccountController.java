package com.ccgg.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ccgg.beans.PrimaryAccount;
import com.ccgg.dao.PrimaryAccountDao;
import com.ccgg.http.Response;
import com.ccgg.service.AccountService;

@RestController
@RequestMapping("/primaryaccount")
public class PrimaryAccountController {
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	PrimaryAccountDao primaryAccountDao;
	
	@GetMapping
	public List<PrimaryAccount> getaccount(){
		return primaryAccountDao.findAll();
	}
	
	
	@PostMapping
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	public Response addPrimaryAccount(@RequestBody PrimaryAccount primaryAccount) {
		return accountService.addPrimaryAccount(primaryAccount);
	}
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
	public Response deletePrimaryAccount(@PathVariable int id) {
		return accountService.deletePrimaryAccount(id);
	}
	
}

	
	
	
	
	
	
	
	
	