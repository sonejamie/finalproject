package com.ccgg.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.ccgg.handler.AccessDeniedHandlerImpl;
import com.ccgg.handler.AuthenticationEntryPointImpl;
import com.ccgg.handler.AuthenticationFailureHandlerImpl;
import com.ccgg.handler.AuthenticationSuccessHandlerImpl;
import com.ccgg.handler.LogoutSuccessHandlerImpl;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	@Autowired
	AuthenticationEntryPointImpl authenticationEntryPointImpl;

	@Autowired
	AccessDeniedHandlerImpl accessDeniedHandlerImpl;
	
	@Autowired
	AuthenticationSuccessHandlerImpl authenticationSuccessHandlerImpl;

	@Autowired
	AuthenticationFailureHandlerImpl authenticationFailureHandlerImpl;

	@Autowired
	LogoutSuccessHandlerImpl logoutSuccessHandlerImpl;

	@Autowired
	UserDetailsService userDetailsService;

	@Override
	protected void configure(HttpSecurity http) throws Exception{
		http.csrf().disable()
			.cors();//cors config.
// 				.and()
			http.authorizeRequests()
			.antMatchers("/index.html", "/account", "/account/{id}/delete", "/transaction", "/transaction/{id}/delete").permitAll()
			.and()//连接用//允许所有访问,若role admin只有amin才能访问
		.exceptionHandling().authenticationEntryPoint(authenticationEntryPointImpl)
			.and()//用到Handler，返回403请求没有登录
		.exceptionHandling().accessDeniedHandler(accessDeniedHandlerImpl)//请求denied
			.and()
		.formLogin()//form data形式传密码
			.permitAll()//任何人都可以登录
			.loginProcessingUrl("/login")
			.successHandler(authenticationSuccessHandlerImpl)//200
			.failureHandler(authenticationFailureHandlerImpl)//401
			.usernameParameter("username").passwordParameter("password")
			.and()
		.logout()
			.permitAll()
			.logoutUrl("/logout")
			.logoutSuccessHandler(logoutSuccessHandlerImpl)
			.and()
		.rememberMe();
	}
	/*
	 *cors support//跨域，前后台端口不同, 相同域名，不同端口
	 */
	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.addAllowedOrigin("http://localhost:4200");//you should only set trusted sit here
// 			e.g. http://localhost:4200 mean only this site can access.4200发送这些请求才能接受，仅对游览器，只有angular能访问
		configuration.setAllowedMethods(Arrays.asList("GET","POST","PUT","DELETE","HEAD","OPTIONS", "PATCH"));
		configuration.addAllowedHeader("*");
		configuration.setAllowCredentials(true);
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
	@Bean
	public PasswordEncoder passwordEncoder() {//密码存数据库加密
		PasswordEncoder encoder = new BCryptPasswordEncoder(11);//加密强度
		return encoder;
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
}


}
