package com.ccgg.beans;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "bank_personal_info")

public class PersonalInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "BANK_PERSONAL_INFO_SEQ_GEN")
	@SequenceGenerator(name = "BANK_PERSONAL_INFO_SEQ_GEN", sequenceName = "BANK_PERSONAL_INFO_SEQ", allocationSize = 1)
	int id;
	@Column
	String name;
	@JsonFormat(pattern="yyyy-MM-dd")
	LocalDate DOB;
	@Column
	String streetaddress;
	@Column
	String city;
	@Column
	String state;
	@Column
	String zipcode;
	@Column
	double salary;
	@Column
	String educationlevel;
	@Column
	int FICO;
	@Column
	double Mortgage;
	@OneToOne
	@JoinColumn(name = "user_id")
	@JsonIgnore
	User user;
	
	public PersonalInfo() {
		super();
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getDOB() {
		return DOB;
	}

	public void setDOB(LocalDate dOB) {
		DOB = dOB;
	}

	public String getStreetaddress() {
		return streetaddress;
	}

	public void setStreetaddress(String streetaddress) {
		this.streetaddress = streetaddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getEducationlevel() {
		return educationlevel;
	}

	public void setEducationlevel(String educationlevel) {
		this.educationlevel = educationlevel;
	}
	
	public int getFICO() {
		return FICO;
	}

	public void setFICO(int fICO) {
		FICO = fICO;
	}

	public double getMortgage() {
		return Mortgage;
	}

	public void setMortgage(double mortgage) {
		Mortgage = mortgage;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "PersonalInfo [id=" + id + ", name=" + name + ", DOB=" + DOB + ", streetaddress=" + streetaddress
				+ ", city=" + city + ", state=" + state + ", zipcode=" + zipcode + ", salary=" + salary
				+ ", educationlevel=" + educationlevel + ", FICO=" + FICO + ", Mortgage=" + Mortgage
				+ ", user=" + user + "]";
	}
	
	

	



	
	




	
	
	
	
	
	
	

}
