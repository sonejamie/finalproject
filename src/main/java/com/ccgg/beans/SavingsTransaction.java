package com.ccgg.beans;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "bank_savings_transaction")
public class SavingsTransaction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SAVINGS_TRANSACTION_SEQ_GEN")
	@SequenceGenerator(name = "SAVINGS_TRANSACTION_SEQ_GEN", sequenceName = "SAVINGS_TRANSACTION_SEQ")
	private int id;
	@Column(name = "date", nullable = false)
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate date;
	@Column(name = "description", nullable = false)
	private String description;
	@Column(name = "type", nullable = false)
	private String type;
	@Column(name = "status", nullable = false)
	private String status;
	@Column(name = "amount", nullable = false)
	private double amount;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "savings_account_id",referencedColumnName = "id")
	@JsonIgnore
	SavingsAccount savingsAccount;
	
	public SavingsTransaction() {
		super();

	}


	public SavingsTransaction(LocalDate date, String description, String type, String status, double amount,
			SavingsAccount savingsAccount) {
		super();
		this.date = date;
		this.description = description;
		this.type = type;
		this.status = status;
		this.amount = amount;
		this.savingsAccount = savingsAccount;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}


	public void setDate(LocalDate date) {
		this.date = date;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public SavingsAccount getSavingsAccount() {
		return savingsAccount;
	}


	public void setSavingsAccount(SavingsAccount savingsAccount) {
		this.savingsAccount = savingsAccount;
	}


	@Override
	public String toString() {
		return "SavingsTransaction [id=" + id + ", date=" + date + ", description=" + description + ", type=" + type
				+ ", status=" + status + ", amount=" + amount + ", savingsAccount=" + savingsAccount + "]";
	}
	
	
	
	

}
