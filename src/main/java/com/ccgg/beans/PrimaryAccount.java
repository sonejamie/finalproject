package com.ccgg.beans;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "bank_primary_account")
public class PrimaryAccount {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "BANK_PRIMARY_ACCOUNT_SEQ_GEN")
	@SequenceGenerator(name = "BANK_PRIMARY_ACCOUNT_SEQ_GEN", sequenceName = "BANK_PRIMARY_ACCOUNT_SEQ", allocationSize = 1)
	private int id; 
	@Column(name = "PrimaryAccountNumber", nullable = false)
	private int PrimaryAccountNumber;
	@Column(name = "PrimaryAccountBalance", nullable = false)
	private BigDecimal PrimaryAccountBalance;
	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	@JsonIgnore
	User user;
	@OneToMany(fetch = FetchType.LAZY)
	private List<PrimaryTransaction> primaryTransactionList;
	
	public PrimaryAccount() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPrimaryAccountNumber() {
		return PrimaryAccountNumber;
	}

	public void setPrimaryAccountNumber(int primaryAccountNumber) {
		PrimaryAccountNumber = primaryAccountNumber;
	}

	public BigDecimal getPrimaryAccountBalance() {
		return PrimaryAccountBalance;
	}

	public void setPrimaryAccountBalance(BigDecimal primaryAccountBalance) {
		PrimaryAccountBalance = primaryAccountBalance;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<PrimaryTransaction> getPrimaryTransactionList() {
		return primaryTransactionList;
	}

	public void setPrimaryTransactionList(List<PrimaryTransaction> primaryTransactionList) {
		this.primaryTransactionList = primaryTransactionList;
	}

	@Override
	public String toString() {
		return "PrimaryAccount [id=" + id + ", PrimaryAccountNumber=" + PrimaryAccountNumber
				+ ", PrimaryAccountBalance=" + PrimaryAccountBalance + ", user=" + user + ", primaryTransactionList="
				+ primaryTransactionList + "]";
	}


	

	

}
