package com.ccgg.beans;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "bank_primary_transaction")
public class PrimaryTransaction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "PRIMARY_TRANSACTION_SEQ_GEN")
	@SequenceGenerator(name = "PRIMARY_TRANSACTION_SEQ_GEN", sequenceName = "PRIMARY_TRANSACTION_SEQ", allocationSize = 1)
	private int id;
	@Column(name = "date", nullable = false)
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate date;
	@Column(name = "description", nullable = false)
	private String description;
	@Column(name = "type", nullable = false)
	private String type;
	@Column(name = "status", nullable = false)
	private String status;
	@Column(name = "amount", nullable = false)
	private double amount;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "primary_account_id", referencedColumnName = "id")
	@JsonIgnore
	PrimaryAccount primaryAccount;
	
	public PrimaryTransaction() {
		super();
		
	}

	public PrimaryTransaction(LocalDate date, String description, String type, String status, double amount,
			PrimaryAccount primaryAccount) {
		super();
		this.date = date;
		this.description = description;
		this.type = type;
		this.status = status;
		this.amount = amount;
		this.primaryAccount = primaryAccount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public PrimaryAccount getPrimaryAccount() {
		return primaryAccount;
	}

	public void setPrimaryAccount(PrimaryAccount primaryAccount) {
		this.primaryAccount = primaryAccount;
	}

	@Override
	public String toString() {
		return "PrimaryTransaction [id=" + id + ", date=" + date + ", description=" + description + ", type=" + type
				+ ", status=" + status + ", amount=" + amount + ", primaryAccount=" + primaryAccount + "]";
	}


	
	

	

	
	
	
	
	

}
