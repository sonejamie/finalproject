package com.ccgg.beans;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "bank_savings_account")
public class SavingsAccount {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "BANK_SAVINGS_ACCOUNT_SEQ_GEN")
	@SequenceGenerator(name = "BANK_SAVINGS_ACCOUNT_SEQ_GEN", sequenceName = "BANK_SAVINGS_ACCOUNT_SEQ")
	private int id; 
	@Column(name = "SavingsAccountNumber", nullable = false)
	private int SavingsAccountNumber;
	@Column(name = "SavingsAccountBalance", nullable = false)
	private BigDecimal SavingsAccountBalance;
	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	@JsonIgnore
	 User user;
	@OneToMany(fetch = FetchType.LAZY)
	private List<SavingsTransaction> savingsTransactionList;
	
	public SavingsAccount() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSavingsAccountNumber() {
		return SavingsAccountNumber;
	}

	public void setSavingsAccountNumber(int savingsAccountNumber) {
		SavingsAccountNumber = savingsAccountNumber;
	}

	public BigDecimal getSavingsAccountBalance() {
		return SavingsAccountBalance;
	}

	public void setSavingsAccountBalance(BigDecimal savingsAccountBalance) {
		SavingsAccountBalance = savingsAccountBalance;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<SavingsTransaction> getSavingsTransactionList() {
		return savingsTransactionList;
	}

	public void setSavingsTransactionList(List<SavingsTransaction> savingsTransactionList) {
		this.savingsTransactionList = savingsTransactionList;
	}

	@Override
	public String toString() {
		return "SavingsAccount [id=" + id + ", SavingsAccountNumber=" + SavingsAccountNumber
				+ ", SavingsAccountBalance=" + SavingsAccountBalance + ", user=" + user + ", savingsTransactionList="
				+ savingsTransactionList + "]";
	}




}
