package com.ccgg.http;

import com.ccgg.beans.PersonalInfo;

public class PersonalInfoResponse extends Response {

	private PersonalInfo personalInfo;

	public PersonalInfoResponse(boolean success, PersonalInfo personalInfo) {
		super(success);
		this.personalInfo = personalInfo;
	}

	public PersonalInfo getUserDetail() {
		return personalInfo;
	}

	public void setPersonalInfo(PersonalInfo personalInfo) {
		this.personalInfo = personalInfo;
	}
	
	
}

