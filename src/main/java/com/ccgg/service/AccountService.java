package com.ccgg.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ccgg.beans.PrimaryAccount;
import com.ccgg.beans.SavingsAccount;
import com.ccgg.dao.PrimaryAccountDao;
import com.ccgg.dao.SavingsAccountDao;
import com.ccgg.http.Response;

@Service
@Transactional
public class AccountService {
	
	@Autowired
	PrimaryAccountDao primaryaccountDao;
	
	@Autowired
	SavingsAccountDao savingsaccountDao;
	
	public Response addPrimaryAccount(PrimaryAccount primaryaccount) {
		primaryaccountDao.save(primaryaccount);
		return new Response(true);
	}
	
	public Response addSavingsAccount(SavingsAccount savingsaccount) {
		savingsaccountDao.save(savingsaccount);
		return new Response(true);
	}
	
	public Response deletePrimaryAccount(int id) {
		if(primaryaccountDao.findById(id)!=null) {
			primaryaccountDao.deleteById(id);
			return new Response(true);
			
		}else{
		return new Response(false, "Account is not found!");
			
		}
	}
	
	public Response deleteSavingsAccount(int id) {
		if(savingsaccountDao.findById(id)!=null) {
			savingsaccountDao.deleteById(id);
			return new Response(true);
			
		}else{
		return new Response(false, "Account is not found!");
			
		}
	}
	
	
	
	
	
	

}
