package com.ccgg.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ccgg.beans.PrimaryTransaction;
import com.ccgg.beans.SavingsTransaction;
import com.ccgg.dao.PrimaryTransactionDao;
import com.ccgg.dao.SavingsTransactionDao;
import com.ccgg.http.Response;

@Service
@Transactional
public class TransactionService {
	
	@Autowired
	PrimaryTransactionDao primaryTransactionDao;
	
	@Autowired
	SavingsTransactionDao savingsTransactionDao;
	
	
	public Response addPrimaryTransaction(PrimaryTransaction primaryTransaction) {
		primaryTransactionDao.save(primaryTransaction);
		return new Response(true);
	} 
	
	public Response addSavingsTransaction(SavingsTransaction savingsTransaction) {
		savingsTransactionDao.save(savingsTransaction);
		return new Response(true);
	} 
	
	public Response updatePrimaryTransaction(PrimaryTransaction primaryTransaction) {
		PrimaryTransaction pt = primaryTransactionDao.findById(primaryTransaction.getId());
		pt.setAmount(primaryTransaction.getAmount());
		pt.setDate(primaryTransaction.getDate());
		pt.setDescription(primaryTransaction.getDescription());
		pt.setStatus(primaryTransaction.getStatus());
		pt.setType(primaryTransaction.getType());
		primaryTransactionDao.save(pt);
		return new  Response(true);
	}
	
	public Response updateSavingsTransaction(SavingsTransaction savingsTransaction) {
		SavingsTransaction st = savingsTransactionDao.findById(savingsTransaction.getId());
		st.setAmount(savingsTransaction.getAmount());
		st.setDate(savingsTransaction.getDate());
		st.setDescription(savingsTransaction.getDescription());
		st.setStatus(savingsTransaction.getStatus());
		st.setType(savingsTransaction.getType());
		savingsTransactionDao.save(st);
		return new  Response(true);
	}
	
	public Response deleteprimaryTransaction(int id) {
		if(primaryTransactionDao.findById(id)!=null) {
			primaryTransactionDao.deleteById(id);
			return new Response(true);
			
		} else {
			return new Response(false, "Transaction is not found!");
		}
	}
	
	public Response deletesavingsTransaction(int id) {
		if(savingsTransactionDao.findById(id)!=null) {
			savingsTransactionDao.deleteById(id);
			return new Response(true);
			
		} else {
			return new Response(false, "Transaction is not found!");
		}
	}
	
	
	
	

}
