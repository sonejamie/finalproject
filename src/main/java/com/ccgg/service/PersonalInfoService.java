package com.ccgg.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ccgg.beans.PersonalInfo;
import com.ccgg.dao.PersonalInfoDao;
import com.ccgg.http.Response;

@Service
@Transactional
public class PersonalInfoService {
	
	@Autowired
	PersonalInfoDao personalInfoDao;
	
	public Response addPersonalInfo(PersonalInfo personalInfo) {
		personalInfoDao.save(personalInfo);
		return new Response(true);
	}
	
	public Response updatePersonalInfo(PersonalInfo personalInfo) {
		PersonalInfo pi = personalInfoDao.findByName(personalInfo.getName());
		pi.setDOB(pi.getDOB());
		pi.setStreetaddress(pi.getStreetaddress());
		pi.setCity(pi.getCity());
		pi.setState(pi.getState());
		pi.setZipcode(pi.getZipcode());
		pi.setSalary(pi.getSalary());
		pi.setEducationlevel(pi.getEducationlevel());
		pi.setFICO(pi.getFICO());
		pi.setMortgage(pi.getMortgage());
		personalInfoDao.save(pi);
		return new Response(true);
	}
	
	public Response deletePersonalInfo(int id) {
		if(personalInfoDao.findById(id)!=null) {
			personalInfoDao.deleteById(id);
			return new Response(true);
		} else {
			return new Response(false, "PersonalInfo is not found!");
		}
	}
}

