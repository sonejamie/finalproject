package com.ccgg.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ccgg.beans.PrimaryTransaction;
@Repository
public interface PrimaryTransactionDao extends JpaRepository<PrimaryTransaction, Integer>{

	PrimaryTransaction findById(int id);
	
	

}
