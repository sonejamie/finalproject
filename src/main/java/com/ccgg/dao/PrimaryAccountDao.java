package com.ccgg.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ccgg.beans.PrimaryAccount;
@Repository
public interface PrimaryAccountDao extends JpaRepository<PrimaryAccount, Integer>{
	
	PrimaryAccount findById (int id);

}
