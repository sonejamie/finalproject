package com.ccgg.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ccgg.beans.SavingsTransaction;

@Repository
public interface SavingsTransactionDao extends JpaRepository<SavingsTransaction, Integer>{
	
	SavingsTransaction findById(int id);

}
