package com.ccgg.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ccgg.beans.PersonalInfo;

@Repository
public interface PersonalInfoDao extends JpaRepository<PersonalInfo, Integer > {
	
	PersonalInfo findByName(String name);

}
