package com.ccgg.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ccgg.beans.SavingsAccount;
@Repository
public interface SavingsAccountDao extends JpaRepository<SavingsAccount, Integer>{
	
	SavingsAccount findById (int id);

}
